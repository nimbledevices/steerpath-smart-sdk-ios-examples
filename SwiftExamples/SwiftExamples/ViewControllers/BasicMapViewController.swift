//
//  BasicMapViewController.swift
//  SwiftExamples
//
//  Created by Jarvis Luong on 11.2.2020.
//  Copyright © 2020 Steerpath. All rights reserved.
//

import UIKit
import SteerpathSmartSDK

class BasicMapViewController: UIViewController {

    
    @IBOutlet weak var mapView: SPSmartMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.setMapMode(.mapOnly)

        // Do any additional setup after loading the view.
    }

}
