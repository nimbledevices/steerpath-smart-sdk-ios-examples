//
//  MarkersViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 10/12/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import SteerpathSmartSDK

//MARK: - MarkersViewController Class Implementation

class MarkersViewController: UIViewController, SPSmartMapViewDelegate {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var mapView : SPSmartMapView!
    
    //MARK: - Object Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
    }
    
    func spSmartMapView(onMapLoaded smartMap: SPSmartMapView) {
        
        /**
            Example 1:
         
            In this example we fetch a map object, move the camera
            position to that map object and add a default marker to that map object.
         
            We want to add a marker to the Kitchen at the Steerpath Office.
            The building reference is "67" and we will use the static source. Static source refers to the map data.
         
            See the SPSmartNavigationView documentation for more marker related methods.
            https://s3-eu-west-1.amazonaws.com/steerpath/ios/releases/smart-sdk-documentation/latest/Classes/SPSmartMapView.html
        */
        self.mapView.getMapObject("Kitchenette", buildingRef: "431", source: .static) { [weak self] (mapObject, objectResponse) in
            //Check if we found the map object
            guard let mapObject = mapObject else {
                return
            }

            self?.mapView.setCameraToObject(mapObject.localRef, buildingRef: mapObject.buildingRef, zoomLevel: 20, completion: { (cameraResponse) in
                if (cameraResponse == .success) {
                    print("Successfully moved camera to ", mapObject.localRef)
                    //Add a marker to the map object
                    self?.mapView.addMarker(mapObject)
                } else {
                    print("Could not move camera to ", mapObject.localRef)
                }
            })
        }
        
        /**
            Example 2:
         
            In this example we add a custom image resource into the map, fetch a map object, move the camera
            position to that map object and add a marker with custom properties.
         
            See the SPSmartMapView documentation for more marker related methods.
            https://s3-eu-west-1.amazonaws.com/steerpath/ios/releases/smart-sdk-documentation/latest/Classes/SPSmartMapView.html
        */
//        let customMarkerImage = UIImage(named: "My_Custom_Marker")
//        guard let myCustomMarkerImage = customMarkerImage else {
//            return
//        }
//        self.mapView.addIconImage("My Custom Marker", image: myCustomMarkerImage)
//
//        self.mapView.getMapObject("Meeting Room", buildingRef: "67", source: .static) { [weak self] (mapObject, objectResponse) in
//            //Check if we found the map object
//            guard let mapObject = mapObject else {
//                return
//            }
//
//            self?.mapView.setCameraToObject(mapObject.localRef, buildingRef: mapObject.buildingRef, zoomLevel: 20, completion: { (cameraResponse) in
//                if (cameraResponse == .success) {
//                    print("Successfully moved camera to ", mapObject.localRef)
//                    //Add custom marker to the map object
//                    self?.mapView.addMarker(mapObject, layout: .top, iconName: "My Custom Marker", textColor: "#ffffff", textHaloColor: "#0000ff")
//                } else {
//                    print("Could not move camera to ", mapObject.localRef)
//                }
//            })
//        }
    }
    
}
