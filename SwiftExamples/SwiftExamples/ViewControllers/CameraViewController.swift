//
//  CameraViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 10/12/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import SteerpathSmartSDK

//MARK: - CameraViewController Class Implementation

class CameraViewController: UIViewController, SPSmartMapViewDelegate {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var mapView : SPSmartMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**
            Example 1:
         
            In this example we set the map coordinates to the Steerpath Office located in Espoo, Finland.
         
            The 'setCamera' method takes a GPS point (latitude, longitude) as parameters as well as the map zoom level.
         
            Zoom level 0 is the whole world map. The bigger the zoom level the closer things appear on the screen.
            For this example we're using zoom level 20 so we can see a close up of the building.
         
            See the SPSmartMapView documentation for more camera related methods.
            https://s3-eu-west-1.amazonaws.com/steerpath/ios/releases/smart-sdk-documentation/latest/Classes/SPSmartMapView.html
        */
        self.mapView.setCamera(60.220949, longitude: 24.812386, zoomLevel: 20)
        
        /**
            Example 2:
         
            In this example we don't know the exact coordinates for the building so we're going to let the SPSmartMapView
            handle finding and displaying it for us.
         
            We only know that that Steerpath Office building reference is "67". We're going to provide that to the 'setCamera' method.
         
            See the SPSmartMapView documentation for more camera related methods.
            https://s3-eu-west-1.amazonaws.com/steerpath/ios/releases/smart-sdk-documentation/latest/Classes/SPSmartMapView.html
        */
//        let buildingRef = "431"
//        self.mapView.setCameraToBuildingRef(buildingRef) { (mapResponse) in
//            if (mapResponse == .success) {
//                print("Successfully moved camera to building ", buildingRef)
//            } else {
//                print("Could not move camera to building ", buildingRef)
//            }
//        }
        
        /**
            Example 3:
         
            In this example we don't know the exact coordinate where our Kitchen is so we're going to let the SPSmartMapView
            handle finding and displaying it for us.
         
            We only know that the Steerpath Office building reference is "67" and it contains a Kitchen. We're going to provide that information
            to the 'setCamera' method.
         
            See the SPSmartMapView documentation for more camera related methods.
            https://s3-eu-west-1.amazonaws.com/steerpath/ios/releases/smart-sdk-documentation/latest/Classes/SPSmartMapView.html
        */
//        let buildingRef = "67"
//        let objectRef = "Kitchen"
//        self.mapView.setCameraToObject(objectRef, buildingRef: buildingRef, zoomLevel: 20) { (mapResponse) in
//            if (mapResponse == .success) {
//                print("Successfully moved camera to ", objectRef)
//            } else {
//                print("Could not move camera to ", objectRef)
//            }
//        }
        
    }

}
