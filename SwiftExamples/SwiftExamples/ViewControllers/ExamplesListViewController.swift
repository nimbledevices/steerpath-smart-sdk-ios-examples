//
//  ExamplesListViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 10/12/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit

//MARK: - Enums

enum Examples : Int {
    case basic = 0
    case camera = 1
    case markers = 2
    case navigation = 3
    case search = 4
    case count = 5
}

//MARK: - ExamplesListViewController Class Implementation

class ExamplesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - Properties
    
    @IBOutlet fileprivate var examplesTableView : UITableView!
    
    //MARK: - Object Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Examples"
        let exampleNib = UINib(nibName: ExampleTableViewCell.nibName, bundle: nil)
        examplesTableView.register(exampleNib, forCellReuseIdentifier: ExampleTableViewCell.identifier)
    }
    
    //MARK: - UITableViewDelegate/DataSource
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ExampleTableViewCell = tableView.dequeueReusableCell(withIdentifier: ExampleTableViewCell.identifier, for: indexPath) as! ExampleTableViewCell
        cell.titleLabel.text = ExamplesListViewController.titleForExample(indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Examples.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: ExamplesListViewController.segueForExample(indexPath.row), sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(ExampleTableViewCell.height)
    }
    
    //MARK: - Helpers
    
    static func titleForExample(_ example : Int) -> String {
        var title = "Example"
        switch example {
        case Examples.basic.rawValue:
            title = "Basic Map Example"
            break
        case Examples.camera.rawValue:
            title = "Camera Example"
            break
        case Examples.markers.rawValue:
            title = "Adding Markers Example"
            break
        case Examples.navigation.rawValue:
            title = "Navigation Example"
            break
        case Examples.search.rawValue:
            title = "Map with search mode example"
            break
        default:
            title = "Example"
        }
        return title
    }
    
    static func segueForExample(_ example : Int) -> String {
        var segue = "ShowCamera"
        switch example {
        case Examples.basic.rawValue:
            segue = "ShowBasicMap"
            break
        case Examples.camera.rawValue:
            segue = "ShowCamera"
            break
        case Examples.markers.rawValue:
            segue = "ShowMarkers"
            break
        case Examples.navigation.rawValue:
            segue = "ShowNavigation"
            break
        case Examples.search.rawValue:
            segue = "ShowSearchMap"
            break
        default:
            segue = "ShowCamera"
        }
        return segue
    }

}
