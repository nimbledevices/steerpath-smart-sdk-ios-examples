//
//  NavigationViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 12/12/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import SteerpathSmartSDK

//MARK: - NavigationViewController Class Implementation

class NavigationViewController: UIViewController, SPSmartMapViewDelegate {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var mapView : SPSmartMapView!

    //MARK: -  Object Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView.delegate = self
    }
    
    func spSmartMapView(onMapLoaded smartMap: SPSmartMapView) {
        let navigationTask = SPSmartMapNavigationUserTask(localRef: "Storage", building: "431")
        let origin = SPSmartMapObject()
        origin.localRef = "Kichenette"
        origin.buildingRef = "431"
        
        navigationTask.setOrigin(origin)
        
        mapView.start(navigationTask)
    }
    
    
    func spSmartMapView(onNavigationStarted smartMap: SPSmartMapView) {
        print("Navigation started")
    }
    
    func spSmartMapView(onNavigationEnded smartMap: SPSmartMapView) {
        print("Navigation ended")
    }
    
    func spSmartMapView(onNavigationFailed smartMap: SPSmartMapView, withError error: SPNavigationError) {
        print("navigation failed with error \(error)")
    }
    

}
