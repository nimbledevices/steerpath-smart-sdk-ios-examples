//
//  ExampleTableViewCell.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 10/12/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit

//MARK: - ExampleTableViewCell Class Implementation

class ExampleTableViewCell: UITableViewCell {
    
    //MARK: - Static
    
    static let identifier = "ExampleTableViewCellIdentifier"
    static let nibName = "ExampleTableViewCell"
    static let height = 44.0

    //MARK: - Properties
    
    @IBOutlet public var titleLabel : UILabel!
}
