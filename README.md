# Steerpath Smart SDK iOS Examples #

Copyright Steerpath Ltd. 2020. All rights reserved

# Folder Structure

"SwiftExamples" contains example Xcode project written in Swift.

# Requirements
* Have CocoaPods installed on your OS X (https://cocoapods.org/)
* Have the necessary API access token provided by Steerpath.
* Don't have API access? Contact: support@steerpath.com

# How to build examples
* Go to either example folder and do the following
```
pod repo update
pod install
```
* Open the generated .xcworkspace with Xcode
* Build!

# Integrating to your app

See instructions at https://bitbucket.org/nimbledevices/steerpath-smart-sdk-podspec

# Contact

support@steerpath.com
